FROM debian:12-slim
MAINTAINER Accomodata <support@accomodata.be>


RUN set -e \
    # Update APT cache & install upgrades
    && apt-get update \
    && apt-get upgrade -y \
    # Install packages
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        bash \
        python3 \
        python3-aiosmtpd \
    # Do cleanup
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    # Done
    && echo 'Done'

VOLUME ["/srv"]
EXPOSE 1024
ADD odoo-lmtpd.py /

CMD ["python3","/odoo-lmtpd.py"]
