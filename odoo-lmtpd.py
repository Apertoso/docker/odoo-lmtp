#!/usr/bin/env python3
import email.parser
import json
import logging
import signal
import xmlrpc.client
from time import sleep

from aiosmtpd.controller import Controller
from aiosmtpd.lmtp import LMTP

_logger = logging.getLogger(__name__)

CRLF = '\r\n'


class ConfigurationError(Exception):
    pass


class RecpientNotFoundError(Exception):
    pass


class LMTPController(Controller):

    def factory(self):
        server = LMTP(self.handler)
        server.__ident__ = 'Accomodata odoo LMTP runner 15.0.0.0.1'
        return server


class OdooEmailHandler:

    def __init__(self, configfile):
        self.configfile = configfile

    def _get_config(self, rcpt_tos):
        try:
            with open(self.configfile, 'r') as config_file_h:
                config_data = json.load(config_file_h)
        except Exception:
            _logger.exception("cannot open config")
            raise ConfigurationError

        config_items = {}
        for rcpt_to in rcpt_tos:
            # everything after the first "@" is the domain part
            domain = '@'.join(rcpt_to.split('@')[1:])

            if rcpt_to in config_data:
                config_item = config_data.get(rcpt_to)
            elif domain in config_data:
                config_item = config_data.get(domain)
            else:
                config_item = None

            config_items.update({rcpt_to: config_item})
        return config_items

    async def handle_DATA(self, server, session, envelope):

        try:
            config_items = self._get_config(envelope.rcpt_tos)
        except ConfigurationError:
            return '451 4.3.5 Cannot open configuration'

        responses = []
        for rcpt_to in envelope.rcpt_tos:
            config_item = config_items.get(rcpt_to)
            responses.append(self.deliver_message(
                config_item, rcpt_to, envelope
            ))
        return CRLF.join(responses)

    def deliver_message(self, config_item, rcpt_to, envelope):

        if config_item is None:
            _logger.error(
                "no config found for rcpt_to '%s'" % (rcpt_to)
            )
            return '550 5.1.1 %s Recipient not found' % rcpt_to

        host = config_item.get('host')
        port = config_item.get('port', 8069)
        database = config_item.get('database')
        userid = config_item.get('userid', 2)
        password = config_item.get('password', 'admin')
        model = config_item.get('model', False)
        always_discard = config_item.get('always_discard', False)
        save_original = config_item.get('save_original', False)
        custom_values = config_item.get('custom_values', False)
        context = config_item.get('context', False)

        if always_discard:
            return '250 %s Message accepted for discard' % rcpt_to

        # ensure host and database is set!
        if not all((host, database)):
            return '451 4.3.5 %s Invalid configuration' % rcpt_to

        parser = email.parser.BytesParser()
        message = parser.parsebytes(envelope.original_content)
        if 'delivered-to' not in message:
            message.add_header('Delivered-To', rcpt_to)
        else:
            message.add_header('X-Original-To', message.get('delivered-to'))
            message.replace_header('Delivered-To', rcpt_to)
        if 'return-path' not in message:
            message.add_header('Return-Path', envelope.mail_from)

        try:
            models = xmlrpc.client.ServerProxy(
                'http://%s:%s/xmlrpc/2/object' % (host, port),
                allow_none=True
            )
            kwargs = {
                'custom_values': custom_values,
                'save_original': save_original,
            }
            if context:
                kwargs.update({'context': context})
            models.execute_kw(
                database, userid, password, 'mail.thread',
                'message_process',
                [model, xmlrpc.client.Binary(message.as_bytes())],
                kwargs,
            )
            return '250 %s Message accepted for delivery' % rcpt_to
        except xmlrpc.client.Fault as e:
            # reformat xmlrpc faults to print a readable traceback
            try:
                err = e.faultString.split('\n')[-2:-1][0]
            except IndexError:
                err = "xmlrpclib.Fault: %s\n%s" % (e.faultCode, e.faultString)
            _logger.exception(
                "Odoo exception: '%s'" % err
            )
            return '550 5.7.1 %s Mailbox unavailable - %s' % (
                rcpt_to, err
            )
        except Exception:
            _logger.exception(
                "system error exception:"
            )
            return '450 %s system error' % rcpt_to


if __name__ == "__main__":

    root_logger = logging.getLogger()
    root_logger.addHandler(logging.StreamHandler())
    root_logger.setLevel(logging.INFO)

    odoo_handler = OdooEmailHandler(configfile="/srv/credentials.json")
    server = LMTPController(
        handler=odoo_handler,
        hostname="",
        port=1024,
    )

    keep_running = True

    def signal_handler(signum, frame):
        _logger.info('Got signal %s, gracefully stopping server' % signum)
        server.stop()
        global keep_running
        keep_running = False

    signal.signal(signal.SIGTERM, signal_handler)

    server.start()
    # sleep forever
    try:
        while keep_running:
            sleep(1)
    except KeyboardInterrupt:
        _logger.info('Time to exit, gracefully stopping server')
        server.stop()
    _logger.info('All done')
