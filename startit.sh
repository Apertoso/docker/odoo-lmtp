#!/bin/bash

docker run \
  --rm \
  -it \
  --name=odoo-lmtp \
  --hostname=odoo-lmtp \
  --network=docker_backend \
  -v $PWD/srv:/srv \
  odoo-lmtp $*
